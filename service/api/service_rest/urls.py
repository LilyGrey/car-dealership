from django.urls import path
from .views import (
    api_technician_list,
    api_technician_detail,
    api_appointment_list,
    api_appointment_detail,
    api_cancel_appointment,
    api_finish_appointment,
)

urlpatterns = [
    path("technicians/", api_technician_list, name="api_technician_list"),
    path("technicians/<int:id>/", api_technician_detail, name="api_technician_detail"),
    path("appointments/", api_appointment_list, name="api_appointment_list"),
    path(
        "appointments/<int:id>/",
        api_appointment_detail,
        name="api_appointment_detail",
    ),
    path(
        "appointments/<int:id>/cancel/",
        api_cancel_appointment,
        name="api_cancel_appointment",
    ),
    path(
        "appointments/<int:id>/finish/",
        api_finish_appointment,
        name="api_finish_appointment",
    ),
]

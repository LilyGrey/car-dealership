from django.shortcuts import render
from .models import Technician, Appointment
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .encoders import TechnicianEncoder, AppointmentEncoder


# Create your views here.
@require_http_methods(["GET", "POST"])
def api_technician_list(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technicians = Technician.objects.create(**content)
            return JsonResponse(
                technicians,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"error": "Could not create technician"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE"])
def api_technician_detail(request, id):
    if request.method == "GET":
        technician = Technician.objects.get(id=id)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
        )
    else:
        try:
            technician = Technician.objects.get(id=id)
            technician.delete()
            return JsonResponse(
                {"message": "Technician deleted successfully!"},
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"error": "Technician does not exist"},
                status=400,
            )


@require_http_methods(["GET", "POST"])
def api_appointment_list(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"error": "Technician does not exist"},
                status=400,
            )

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_appointment_detail(request, id):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
        )
    else:
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.delete()
            return JsonResponse(
                {"message": "Appointment deleted successfully!"},
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"error": "Appointment does not exist"},
                status=400,
            )


@require_http_methods(["PUT"])
def api_finish_appointment(request, id):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.status = "Finished"
            appointment.save()
            return JsonResponse(
                {"message": "Appointment finished successfully!"},
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"error": "Appointment does not exist"},
                status=400,
            )


@require_http_methods(["PUT"])
def api_cancel_appointment(request, id):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.status = "Cancelled"
            appointment.save()
            return JsonResponse(
                {"message": "Appointment cancelled successfully!"},
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"error": "Appointment does not exist"},
                status=400,
            )

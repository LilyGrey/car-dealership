import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

export default function CreateManufacturer() {
	const navigate = useNavigate();
	const [manufacturers, setManufacturers] = useState([]);
	const [name, setName] = useState("");

	const handleNameChange = (event) => {
		setName(event.target.value);
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {
			name: name,
		};

		const url = "http://localhost:8100/api/manufacturers/";
		const fetchConfig = {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify(data),
		};

		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			const data = await response.json();
			setManufacturers([...manufacturers, data]);
			setName("");

			navigate("/manufacturers");
		}
	};

	const getData = async () => {
		const url = "http://localhost:8100/api/manufacturers/";
		const response = await fetch(url);
		const data = await response.json();
		setManufacturers(data.manufacturers);
	};

	useEffect(() => {
		getData();
	}, []);

	return (
		<>
			<div className="row">
				<div className="offset-3 col-6">
					<div className="shadow p-4 mt-4">
						<h1>Create a Manufacturer</h1>
						<form
							onSubmit={handleSubmit}
							id="create-manufacturer-form"
						>
							<div className="form-floating mb-3">
								<input
									value={name}
									onChange={handleNameChange}
									placeholder="Name"
									required
									type="text"
									name="name"
									id="name"
									className="form-control"
								/>
								<label htmlFor="name">Name</label>
							</div>
							<button type="submit" className="btn btn-success">
								Create
							</button>
						</form>
					</div>
				</div>
			</div>
		</>
	);
}
